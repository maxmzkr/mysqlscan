package e2e

import (
	"fmt"
	"net"
	"os/exec"
	"regexp"
	"testing"
	"time"
)

func TestE2E(t *testing.T) {
	for i := 0; i < 10; i++ {
		if _, err := net.DialTimeout("tcp", "msyql:3306", time.Duration(1)*time.Second); err != nil {
			time.Sleep(time.Duration(1) * time.Second)
			continue
		}
		if _, err := net.DialTimeout("tcp", "msyql_old:3306", time.Duration(1)*time.Second); err != nil {
			time.Sleep(time.Duration(1) * time.Second)
			continue
		}
		if _, err := net.DialTimeout("tcp", "postgres:5432", time.Duration(1)*time.Second); err != nil {
			time.Sleep(time.Duration(1) * time.Second)
			continue
		}
	}

	// This would be easier to test if this returned JSON or something.
	tests := []struct {
		host   string
		port   string
		output *regexp.Regexp
	}{
		{"mysql",
			"3306",
			regexp.MustCompile(regexp.QuoteMeta(
				`This is a MySQL server!
Version: 8.0.22
SSL Enabled: true
Certs: [MySQL_Server_8.0.22_Auto_Generated_Server_Certificate MySQL_Server_8.0.22_Auto_Generated_CA_Certificate]`))},
		{"mysql_old",
			"3306",
			regexp.MustCompile(regexp.QuoteMeta(
				`This is a MySQL server!
Version: 5.5.62
SSL Enabled: false
Certs: []`))},
		{"fake_host",
			"3306",
			regexp.MustCompile("Not a MySQL server")},
		{"postgres",
			"5432",
			regexp.MustCompile("Not a MySQL server failed to get size")},
	}
	for _, tt := range tests {
		testname := fmt.Sprintf("%v,%v", tt.host, tt.port)
		t.Run(testname, func(t *testing.T) {
			out, err := exec.Command("go", "run", "../cmd/mysqlscan/main.go", "-host", tt.host, "-port", tt.port).CombinedOutput()
			if err != nil {
				t.Errorf("Got error from command %v with output %v", err, string(out))
			} else {
				if !tt.output.Match(out) {
					t.Errorf("%v did not match expected %v", string(out), tt.output)
				}
			}
		})
	}
}
