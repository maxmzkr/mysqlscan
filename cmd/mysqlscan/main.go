package main

import (
	"flag"
	"fmt"

	"bitbucket.org/maxmzkr/mysqlscan/pkg/mysqlscan"
)

func main() {
	host := flag.String("host", "mysql", "Host to connect to. IP or Hostname.")
	port := flag.Int("port", 3306, "Port to connect to")
	flag.Parse()

	mysqlDetails, err := mysqlscan.ScanMySQL(*host, *port)
	if err != nil {
		fmt.Printf("Not a MySQL server %v\n", err)
		return
	}

	fmt.Printf("This is a MySQL server!\n")
	fmt.Printf("Version: %v\n", mysqlDetails.ServerVer)
	fmt.Printf("SSL Enabled: %v\n", mysqlDetails.SSLEnabled)
	fmt.Printf("Certs: %v\n", mysqlDetails.Certs)
}
