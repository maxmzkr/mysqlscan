// Package mysqlscan provides a utlity function to get details about a MySQL
// server.
package mysqlscan

import (
	"bufio"
	"crypto/tls"
	"fmt"
	"io"
	"net"
	"time"

	"github.com/hashicorp/go-version"
)

type mysqlDetails struct {
	ServerVer  *version.Version
	SSLEnabled bool
	Certs      []string
}

// https://dev.mysql.com/doc/internals/en/capability-flags.html#packet-Protocol::CapabilityFlags
const (
	_ uint32 = 1 << iota
	_
	_
	_
	_
	_
	_
	_
	_
	_
	_
	CLIENTSSL
	_
	_
	_
	CLIENTSECURECONNECTION
	_
	_
	_
	CLIENTPLUGINAUTH
)

// parsePacket returns the first packet received from MySQL or an error if it's
// already clear that it's not MySQL.
func parsePacket(conn net.Conn) ([]byte, error) {
	sizeBuf := make([]byte, 3)
	reader := bufio.NewReader(conn)
	if _, err := io.ReadFull(reader, sizeBuf); err != nil {
		return nil, fmt.Errorf("failed to get size")
	}
	size := int32(uint32(sizeBuf[0]) + uint32(sizeBuf[1])<<8 + uint32(sizeBuf[2])<<16)

	if seq, err := reader.ReadByte(); err != nil || seq != 0 {
		return nil, fmt.Errorf("bad sequence")
	}

	// Reading all of the handshake first is a good way to confirm the size
	// was actually the size. Better to verify it first than trying to read
	// the rest of the packet field by field.
	handshakeBuf := make([]byte, size)
	if _, err := io.ReadFull(reader, handshakeBuf); err != nil {
		return nil, fmt.Errorf("failed to get all of the handshake")
	}
	return handshakeBuf, nil
}

// parseHandshake accepts a connection and returns information about the MySQL
// server if there is a MySQL server on the other end. If there is not,
// parseHandshake returns an error.
func parseHandshake(handshakeBuf []byte) (*mysqlDetails, error) {
	idx := 0
	if idx >= len(handshakeBuf) {
		return nil, fmt.Errorf("reached end of handshake right at the beginning")
	}
	handshakeVer := handshakeBuf[0]
	if handshakeVer != 9 && handshakeVer != 10 {
		// Or not a supported version. But we'll pretend it's not MySQL
		// for now. The MySQL handshake page doesn't have the other
		// versions immediately available. This seems fine enough for a
		// POC.
		return nil, fmt.Errorf("bad handshake ver")
	}
	idx += 1
	if idx >= len(handshakeBuf) {
		return nil, fmt.Errorf("reached end just after protocol version")
	}

	// Looking for the end of a null terminated string. MySQL docs state
	// that this won't work for broken v5.5.10 and v5.6.2 which don't
	// null terminate. I would probably fix this in a prod solution.
	versionEndIndex := 0
	nullFound := false
	for i, el := range handshakeBuf[idx:] {
		versionEndIndex = i
		if el == '\x00' {
			nullFound = true
			break
		}
	}
	if !nullFound {
		return nil, fmt.Errorf("version string was not null terminated")
	}
	// I'm not certain MySQL uses semver but this will be fine for POC.
	serverVer, err := version.NewVersion(string(handshakeBuf[idx : idx+versionEndIndex]))
	if err != nil {
		return nil, fmt.Errorf("version does not work with semver")
	}
	idx += versionEndIndex + 1
	if idx >= len(handshakeBuf) {
		return nil, fmt.Errorf("reached end just after server version")
	}

	// Connection id. Not important
	idx += 4
	if idx >= len(handshakeBuf) {
		return nil, fmt.Errorf("reached end just after connection id")
	}

	// auth data
	if handshakeVer == 9 {
		for _, el := range handshakeBuf[idx:] {
			idx += 1
			if el == '\x00' {
				break
			}
		}
	} else if handshakeVer == 10 {
		// auth data
		idx += 8
		// filler
		idx += 1
	}

	sslEnabled := false
	if idx < len(handshakeBuf) && handshakeVer == 10 {
		capabilities := uint32(0)
		idx += 1
		if idx >= len(handshakeBuf) {
			return nil, fmt.Errorf("reached end just after capability byte 1")
		}
		capabilityByte2 := handshakeBuf[idx]
		capabilities |= uint32(capabilityByte2) << 8
		sslEnabled = capabilities&CLIENTSSL != 0
		idx += 1

		// character set
		idx += 1
		// status flag
		idx += 2
		if idx >= len(handshakeBuf) {
			return nil, fmt.Errorf("reached end just after status flag")
		}

		capabilityByte3 := handshakeBuf[idx]
		capabilities |= uint32(capabilityByte3) << 16
		clientPluginAuthEnabled := capabilities&CLIENTPLUGINAUTH != 0
		authPluginDataLen := uint8(13)
		idx += 2
		if idx >= len(handshakeBuf) {
			return nil, fmt.Errorf("reached end just after second set of capability bytes")
		}
		if !clientPluginAuthEnabled && handshakeBuf[idx] != 0 {
			// MySQL docs seem to imply that this would be 0 if
			// client plugin auth wasn't enabled. I'd probably want
			// a stat or something here to see if this can be relied
			// on.
			return nil, fmt.Errorf("invalid client plugin length for non enabled client plugin")
		} else if clientPluginAuthEnabled && handshakeBuf[idx] < 8 {
			// The docs make it seem like this includes the 8 bytes
			// that are always in the handshake. The docs don't
			// explicitly state that this will always be over 8.
			// This is another place I would want to have a stat to
			// determine if this is reliable or not.
			return nil, fmt.Errorf("client plugin length too short")
		} else {
			// It seems like this is always padded out to 13 bytes?
			// Little hard to tell from the docs.
			setHandshakeLen := handshakeBuf[idx] - 8
			if setHandshakeLen > 13 {
				authPluginDataLen = setHandshakeLen
			}
		}
		idx += 1

		// reserved 10 null bytes
		idx += 10
		if len(handshakeBuf) <= idx {
			return nil, fmt.Errorf("not enough zero fill")
		}
		for _, el := range handshakeBuf[idx-10 : idx] {
			if el != '\x00' {
				return nil, fmt.Errorf("null bytes expected but they were something different %v", el)
			}
		}
		clientSecureConEnabled := capabilities&CLIENTSECURECONNECTION != 0
		// rest of auth plugin data
		// There is something interesting here. Probably some relationship
		// between this flag and clientPluginAuth
		if clientSecureConEnabled {
			idx += int(authPluginDataLen)
		}
		if idx >= len(handshakeBuf) {
			return nil, fmt.Errorf("reached end just after second bit of auth plugin data")
		}

		// Client plugin name
		if clientPluginAuthEnabled {
			nullFound := false
			for _, el := range handshakeBuf[idx:] {
				idx += 1
				if el == '\x00' {
					nullFound = true
					break
				}
			}
			if !nullFound {
				return nil, fmt.Errorf("did not find end of client plugin auth name")
			}
		}
	}
	if idx != len(handshakeBuf) {
		return nil, fmt.Errorf("unexpected more data after client plugin auth name")
	}

	return &mysqlDetails{ServerVer: serverVer, SSLEnabled: sslEnabled, Certs: nil}, nil
}

// addSSLDetails populates the certs field of the mysqlDetails.
func addSSLDetails(conn net.Conn, details *mysqlDetails) error {
	// Important bit is just the SSL enable flag. The rest is packet
	// boilerplate.
	conn.Write([]byte{'\x20', '\x00', '\x00', '\x01', '\x05', '\xae', '\x03', '\x00', '\x00', '\x00', '\x00', '\x01', '\x08'})
	conn.Write(make([]byte, 23))

	tlsConf := tls.Config{InsecureSkipVerify: true}
	tlsClient := tls.Client(conn, &tlsConf)
	if err := tlsClient.Handshake(); err != nil {
		// At some point this is just a mis configured MySQL server.
		// I'm not distinguishing between mis configured an not a
		// server. Would probably want to distinguish depending on
		// the use case.
		return fmt.Errorf("TLS handshake failed even though SSL was enabled on the server %v", err)
	} else {
		certs := make([]string, len(tlsClient.ConnectionState().PeerCertificates))
		for i, cert := range tlsClient.ConnectionState().PeerCertificates {
			// Probably more interesting things to extract from
			// certs but this is one of the only fields populated
			// by my test env.
			certs[i] = cert.Subject.CommonName
		}
		details.Certs = certs
	}
	return nil
}

// ScanMySQL accepts a host and port and returns details about it if there is a
// MySQL server on the other end and returns and error if it determines there
// is not a MySQL server on the other end.
func ScanMySQL(host string, port int) (*mysqlDetails, error) {
	connStr := fmt.Sprintf("%v:%v", host, port)
	conn, err := net.DialTimeout("tcp", connStr, time.Duration(10)*time.Second)
	if err != nil {
		return nil, err
	}
	conn.SetReadDeadline(time.Now().Add(time.Duration(10) * time.Second))

	handshakeBuf, err := parsePacket(conn)
	if err != nil {
		return nil, err
	}

	details, err := parseHandshake(handshakeBuf)
	if err != nil {
		return nil, err
	}

	if details.SSLEnabled {
		if err := addSSLDetails(conn, details); err != nil {
			return nil, err
		}
	}

	return details, nil
}
