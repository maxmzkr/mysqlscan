package mysqlscan

import (
	"fmt"
	"net"
	"testing"
	"time"

	"github.com/hashicorp/go-version"
)

func TestParsePacket(t *testing.T) {
	fmt.Println("Test!")
	tests := []struct {
		header    []byte
		data      []byte
		shouldErr bool
		desc      string
	}{
		{[]byte{
			'\x00', '\x00', '\x00', // len
			'\x00', //seq
		}, []byte{}, false, "empty packet should be handled"},
		{[]byte{'\x01', '\x00', '\x00', //len
			'\x00', // seq
		}, []byte{'a'}, false, "single element should be handled"},
		{[]byte{'\x01', '\x02', '\x03', // len
			'\x00', //seq
		}, make([]byte, 197121), false, "all three bytes of length should be handled"},
		{[]byte{'\x02', '\x00'}, []byte{}, true, "length field too short"},
		{[]byte{'\x02', '\x00', '\x00'}, // len
			[]byte{}, true, "no sequence"},
		{[]byte{'\x02', '\x00', '\x00', // len
			'\x00', // seq
		}, []byte{'a'}, true, "too long of length should error"},
		{[]byte{'\x01', '\x00', '\x00', // len
			'\x01', // seq
		}, []byte{'a'}, true, "non zero sequence should error"},
	}

	for _, tt := range tests {
		testname := fmt.Sprintf("header: %v, data: %v %v", tt.header, tt.data, tt.desc)
		t.Run(testname, func(t *testing.T) {
			fmt.Println("Test!")
			reader, writer := net.Pipe()
			writer.SetWriteDeadline(time.Now().Add(time.Duration(1) * time.Second))
			go func() {
				writer.Write(tt.header)
				writer.Write(tt.data)
				writer.Close()
			}()
			handshakeBuf, err := parsePacket(reader)
			if tt.shouldErr {
				if handshakeBuf != nil || err == nil {
					t.Errorf("Expected an error and got %v %v", handshakeBuf, err)
				}
			} else {
				if err != nil {
					t.Errorf("Expected data and got an err %v", err)
				} else if len(tt.data) != len(handshakeBuf) {
					t.Errorf("Expected data but got wrong length back %v", handshakeBuf)
				} else {
					for i := range tt.data {
						if tt.data[i] != handshakeBuf[i] {
							t.Errorf("Expected data but got wrong value back %v", handshakeBuf)
						}
					}
				}
			}
		})
	}
}

func QuickVersion(verStr string) *version.Version {
	ver, _ := version.NewVersion(verStr)
	return ver
}

func TestParseHandshake(t *testing.T) {
	fmt.Println("Test!")
	tests := []struct {
		data    []byte
		details *mysqlDetails
		desc    string
	}{
		{[]byte{}, nil, "empty packet should err"},
		{[]byte{'\x01'}, // handshake ver

			nil, "invalid protocol ver should err"},
		{[]byte{byte(10)}, // handshake ver
			nil, "end just after protocol ver should err"},
		{[]byte{byte(10), // handshake ver
			'5', '.', '5', // server ver
		}, nil, "non null terminated version"},
		{[]byte{byte(10), // handshake ver,
			'a', 'v', 'e', 'r', '\x00', // server ver
		}, nil, "invalid version should err"},
		{[]byte{byte(10), // handshake ver
			'5', '.', '5', '\x00', // server ver
		}, nil, "end just after version should err"},
		{[]byte{byte(10), // handshake ver
			'5', '.', '5', '\x00', // server ver
			'\x00', '\x00', '\x00', '\x01', // connection id
		}, nil, "end just after connection id should err"},
		{[]byte{byte(9), // handshake ver
			'5', '.', '5', '\x00', // server ver
			'\x00', '\x00', '\x00', '\x01', // connection id
			'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', '\x00', // auth plugin data
		}, &mysqlDetails{ServerVer: QuickVersion("5.5.0"), SSLEnabled: false, Certs: nil}, "v9 shold return details"},
		{[]byte{byte(9), // handshake ver
			'5', '.', '5', '\x00', // server ver
			'\x00', '\x00', '\x00', '\x01', // connection id
			'\x00', // auth plugin data
		}, &mysqlDetails{ServerVer: QuickVersion("5.5.0"), SSLEnabled: false, Certs: nil}, "v9 with short auth data should return details"},
		{[]byte{byte(9), // handshake ver
			'5', '.', '5', '\x00', // server ver
			'\x00', '\x00', '\x00', '\x01', // connection id
			'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', '\x00', // auth plugin data
		}, &mysqlDetails{ServerVer: QuickVersion("5.5.0"), SSLEnabled: false, Certs: nil}, "v9 with long auth data should return details"},
		{[]byte{byte(10), // handshake ver
			'5', '.', '5', '\x00', // server ver
			'\x00', '\x00', '\x00', '\x01', // connection id
			'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', '\x00', // auth plugin data
		}, &mysqlDetails{ServerVer: QuickVersion("5.5.0"), SSLEnabled: false, Certs: nil}, "end just after auth data should return details"},
		{[]byte{byte(10), // handshake ver
			'5', '.', '5', '\x00', // server ver
			'\x00', '\x00', '\x00', '\x01', // connection id
			'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', '\x00', // auth plugin data
			'\x00', // capability byte 1
		}, nil, "end just after capabilityByte1 should error"},
		{[]byte{byte(10), // handshake ver
			'5', '.', '5', '\x00', // server ver
			'\x00', '\x00', '\x00', '\x01', // connection id
			'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', '\x00', // auth plugin data
			'\x00',         // capability byte 1
			'\x00',         // capability byte 2
			'\x00',         // character set
			'\x00', '\x00', // status
		}, nil, "end just after status flag should error"},
		{[]byte{byte(10), // handshake ver
			'5', '.', '5', '\x00', // server ver
			'\x00', '\x00', '\x00', '\x01', // connection id
			'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', '\x00', // auth plugin data
			'\x00',         // capability byte 1
			'\x00',         // capability byte 2
			'\x00',         // character set
			'\x00', '\x00', // status
			'\x00', '\x00', // capability byte 3 & 4
		}, nil, "end just after capability flag should error"},
		{[]byte{byte(10), // handshake ver
			'5', '.', '5', '\x00', // server ver
			'\x00', '\x00', '\x00', '\x01', // connection id
			'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', '\x00', // auth plugin data
			'\x00',         // capability byte 1
			'\x00',         // capability byte 2
			'\x00',         // character set
			'\x00', '\x00', // status
			'\x00', '\x00', // capability byte 3 & 4
			'\xff', // length
		}, nil, "disabled auth plugin with plugin length should error"},
		{[]byte{byte(10), // handshake ver
			'5', '.', '5', '\x00', // server ver
			'\x00', '\x00', '\x00', '\x01', // connection id
			'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', '\x00', // auth plugin data
			'\x00',         // capability byte 1
			'\x00',         // capability byte 2
			'\x00',         // character set
			'\x00', '\x00', // status
			'\x08', '\x00', // capability byte 3 & 4
			'\x07', // length
		}, nil, "too short of auth plugin should error"},
		{[]byte{byte(10), // handshake ver
			'5', '.', '5', '\x00', // server ver
			'\x00', '\x00', '\x00', '\x01', // connection id
			'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', '\x00', // auth plugin data
			'\x00',         //capability byte 1
			'\x00',         // capability byte 2
			'\x00',         // character set
			'\x00', '\x00', // status
			'\x08', '\x00', // capability byte 3 & 4
			'\x08',                                                         // length
			'\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', // zero fill
		}, nil, "zero fill was too small"},
		{[]byte{byte(10), // handshake ver
			'5', '.', '5', '\x00', // server ver
			'\x00', '\x00', '\x00', '\x01', // connection id
			'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', '\x00', // auth plugin data
			'\x00',         // capability byte 1
			'\x00',         // capability byte 2
			'\x00',         // character set
			'\x00', '\x00', // status
			'\x08', '\x00', // capability byte 3 & 4
			'\x08',                                                                         // length
			'\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\xa0', '\x00', '\x00', '\x00', // zero fill
		}, nil, "bad zero fill should error"},
		{[]byte{byte(10), // handshake ver
			'5', '.', '5', '\x00', // server ver
			'\x00', '\x00', '\x00', '\x01', // connection id
			'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', '\x00', // auth plugin data
			'\x00',         // capability byte 1
			'\x00',         // capability byte 2
			'\x00',         // character set
			'\x00', '\x00', // status
			'\x08', '\x00', // capability byte 3 & 4
			'\x08',                                                                         // length
			'\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', // zero fil
			'a', 'b', // plugin name
		}, nil, "plugin name needs to be null terminated"},
		{[]byte{byte(9), // handshake ver
			'5', '.', '5', '\x00', // server ver
			'\x00', '\x00', '\x00', '\x01', // connection id
			'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', '\x00', // auth plugin data
			'\x00',         // capability byte 1
			'\x00',         // capability byte 2
			'\x00',         // character set
			'\x00', '\x00', // status
			'\x08', '\x00', // capability byte 3 & 4
			'\x08',                                                                         // length
			'\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', // zero fill
			'a', 'b', '\x00', // plugin name
		}, nil, "v9 should end after auth plugin data"},
		{[]byte{byte(10), // handshake ver
			'5', '.', '5', '\x00', // server ver
			'\x00', '\x00', '\x00', '\x01', // connection id
			'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', '\x00', // auth plugin data
			'\x00',         // capability byte 1
			'\x00',         // capability byte 2
			'\x00',         // character set
			'\x00', '\x00', // status
			'\x08', '\x00', // capability byte 3 & 4
			'\x08',                                                                         // length
			'\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', // zero fill
			'a', 'b', '\x00', // plugin name
		}, &mysqlDetails{ServerVer: QuickVersion("5.5.0"), SSLEnabled: false, Certs: nil}, "proper auth plugin data should return details"},
		{[]byte{byte(10), // handshake ver
			'5', '.', '5', '\x00', // server ver
			'\x00', '\x00', '\x00', '\x01', // connection id
			'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', '\x00', // auth plugin data
			'\x00',         // capability byte 1
			'\x08',         // capability byte 2
			'\x00',         // character set
			'\x00', '\x00', // status
			'\x08', '\x00', // capability byte 3 & 4
			'\x08',                                                                         // length
			'\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', '\x00', // zero fill
			'a', 'b', '\x00', // plugin name
		}, &mysqlDetails{ServerVer: QuickVersion("5.5.0"), SSLEnabled: true, Certs: nil}, "SSLEnabled should be handled fine"},
	}

	for _, tt := range tests {
		testname := fmt.Sprintf("data: %v %v %v", tt.data, tt.details, tt.desc)
		t.Run(testname, func(t *testing.T) {
			fmt.Println("Test!")
			mysqlDetails, err := parseHandshake(tt.data)
			if tt.details == nil {
				if mysqlDetails != nil || err == nil {
					t.Errorf("Expected an error and got %v %v", mysqlDetails, err)
				}
			} else {
				if err != nil {
					t.Errorf("Expected data and got an err %v", err)
				} else if !tt.details.ServerVer.Equal(mysqlDetails.ServerVer) {
					t.Errorf("Versions were incorrect %v", mysqlDetails)
				} else if tt.details.SSLEnabled != mysqlDetails.SSLEnabled {
					t.Errorf("SSLEnabled flags were not similar")
				}
			}
		})
	}
}
