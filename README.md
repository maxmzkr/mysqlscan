Tested with Go 1.15.6

This application determines if a given host/port is running a MySQL server. It does this by opening a TCP connection, listening for the handshake packet that a MySQL server sends and verifying that the packet is a MySQL handshake packet. When it receives this packet, it notes down interesting information abut the server, such as the version and if SSL is enabled. If SSL is enabled, then the TLS handshake is performed and the "certs" are recorded. "Certs" is quoted because because the common name is actually recorded because that's the only interesting part filled in by the MySQL docker container. Any error along the way implies that this server is not a MySQL server.

The application can be used by cloning down this repo and then running
```
go run ./cmd/mysqlscan/main.go -host <host> -port <port>
```

The response should look something like one of the two
```
❯ go run ./cmd/mysqlscan/main.go -host 172.20.0.2 -port 3306
This is a MySQL server!
Version: 8.0.22
SSL Enabled: true
Certs: [MySQL_Server_8.0.22_Auto_Generated_Server_Certificate MySQL_Server_8.0.22_Auto_Generated_CA_Certificate]
```

```
❯ go run ./cmd/mysqlscan/main.go -host 172.20.0.2 -port 3307
Not a MySQL server dial tcp 172.20.0.2:3307: connect: connection refused
```

This application comes with a docker-compose file for testing purposes. It has two MySQL servers on it. One running the latest version of MySQL and the other running MySQL 5.5. The newer version has SSL enabled and the older does not. There is also a postgres server running for validating against a server that isn't MySQL. To use this docker container, install docker-compose https://docs.docker.com/compose/install/ and then run
```
❯ docker-compose up                                                                                                                                                                                                
```
There will be a lot of output. The important bit will come from the docker container actually running the tests. If the tests all pass, you should see a line like the following
```
mysqlscan_app_1 exited with code 0
```

Unit tests can also be run locally with the following.
```
go test ./pkg/...
```

End to end tests live in `./test` and will only work in the docker compose environment.
